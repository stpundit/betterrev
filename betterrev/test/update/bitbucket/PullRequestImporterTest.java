package update.bitbucket;

import models.AbstractPersistenceIntegrationTest;
import models.PullReview;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class PullRequestImporterTest extends AbstractPersistenceIntegrationTest {

    private static final String repositoryId = "better-test-repo";

    private static JsonNode firstResponse;
    private static JsonNode secondResponse;

    @BeforeClass
    public static void loadResponses() {
        firstResponse = loadJson("conf/pullrequests_api_sample");
        secondResponse = loadJson("conf/pullrequests_api_sample_updated");
    }

    @Before
    public void dropEntries() {
        for (PullReview review : PullReview.find.all())
            review.delete();
    }

    private static JsonNode loadJson(String location) {
        File file = new File(location);
        assertTrue(file.exists());
        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper();
        try (JsonParser parser = factory.createJsonParser(file)) {
            return mapper.readTree(parser);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void importsSample() {
        List<PullReview> reviews = PullRequestImporter.importAllReviews(firstResponse, repositoryId);

        PullReview review = reviews.get(0);
        checkData(review, "code to be pull requested", "this is a test pull request, please don't merge or close it.", "2013-04-10T18:44:01+00:00");
        assertPullReviewEquals(review, repositoryId, "1", "code to be pull requested",
                "this is a test pull request, please don't merge or close it.");
        assertRequesterInfo(review, "richardwarburton", "Richard Warburton");
    }

    @Test
    public void importAllReviews_validRepo_fullUrlIsBuiltSuccessfully() {
        List<PullReview> reviews = PullRequestImporter.importAllReviews(firstResponse, repositoryId);

        PullReview review = reviews.get(0);

        assertThat(review.pullRequestUrl(), is(String.format("https://bitbucket.org/%s/%s/pull-request/%s/diff",
                "richardwarburton", "better-test-repo", "1")));
    }

    @Test
    public void todo() {
        List<PullReview> reviews = PullRequestImporter.importAllReviews(firstResponse, repositoryId);

        PullReview review = reviews.get(0);

        assertThat(review.pullRequestUrl(), is(String.format("https://bitbucket.org/%s/%s/pull-request/%s/diff",
                "richardwarburton", "better-test-repo", "1")));
    }

    @Test
    public void updatesReview() {
        // Load the old version of the pull review, so there's something to update
        PullRequestImporter.importAllReviews(firstResponse, repositoryId);

        PullReview oldReview = PullReview.findByBitbucketIds(repositoryId, "1");
        PullReview newReview = PullRequestImporter.importAllReviews(secondResponse, repositoryId).get(0);

        assertEquals(oldReview.id, newReview.id);
        checkData(newReview, "code to be pull requested come on", "this is an updated pull request.", "2013-05-10T18:44:01+00:00");
    }

    private void checkData(PullReview review, String name, String description, String updatedAt) {
        assertPullReviewEquals(review, repositoryId, "1", name, description);
        assertTimePoints(review, DateTime.parse("2013-04-05T18:32:07+00:00"),
                DateTime.parse(updatedAt));
    }

    private void assertTimePoints(PullReview review, DateTime expectedCreated, DateTime expectedUpdated) {
        assertEquals(expectedCreated.getMillis(), review.createdOn.getMillis());
        assertEquals(expectedUpdated.getMillis(), review.updatedOn.getMillis());
    }

    private void assertPullReviewEquals(PullReview review, String repoId, String pullRequestId, String name, String description) {
        assertEquals(repoId, review.repositoryId);
        assertEquals(pullRequestId, review.pullRequestId);
        assertEquals(name, review.name);
        assertEquals(description, review.description);
    }

    private void assertRequesterInfo(PullReview review, String bitbucketName, String displayName) {
        assertEquals(bitbucketName, review.requester.bitbucketUserName);
        assertEquals(displayName, review.requester.name);
    }

}
