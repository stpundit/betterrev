package update.external;

import models.*;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class PullReviewModifierTest extends AbstractPersistenceIntegrationTest {

    public static final String TEST_LINK_TO_EXTERNAL_INFO = "linkToExternalInfo";

    @Before
    public void setUp() {
        for (PullReview pullReview : PullReview.find.all()) {
            pullReview.delete();
        }
    }

    @Test
    public void rejectedPullReviewHasLinkToExternalInfo() {
        PullReview pullReview = PullReviewTest.createTestInstance(State.PENDING_APPROVAL);

        pullReview = PullReviewModifier.rejectPullReview(pullReview.repositoryId, pullReview.pullRequestId,
                TEST_LINK_TO_EXTERNAL_INFO);

        String linkToExternalInfo = null;
        for (PullReviewEvent pullReviewEvent : pullReview.pullReviewEvents) {
            if (pullReviewEvent.pullReviewEventType != null &&
                    pullReviewEvent.pullReviewEventType.equals(PullReviewEventType.REJECTED)) {
                linkToExternalInfo = pullReviewEvent.linkToExternalInfo;
                break;
            }
        }
        assertThat("Link to External Info is not set correctly", linkToExternalInfo, is(TEST_LINK_TO_EXTERNAL_INFO));
    }


    @Test
    public void rejectedPullReviewContainsAppropriateEvent() {
        PullReview pullReview = PullReviewTest.createTestInstance(State.PENDING_APPROVAL);

        pullReview = PullReviewModifier.rejectPullReview(pullReview.repositoryId, pullReview.pullRequestId,
                TEST_LINK_TO_EXTERNAL_INFO);

        assertTrue("Could not find Rejected PullReviewEventType",
                pullReviewContainsEvent(pullReview, PullReviewEventType.REJECTED));
    }


    @Test
    public void mergedPullReviewHasAppropriateEvent() {
        PullReview pullReview = PullReviewTest.createTestInstance(State.ACCEPTED);

        pullReview = PullReviewModifier.mergePullReview(pullReview.repositoryId, pullReview.pullRequestId,
                TEST_LINK_TO_EXTERNAL_INFO);

        assertTrue("Could not find Merged PullReviewEventType",
                pullReviewContainsEvent(pullReview, PullReviewEventType.MERGED));
    }


    @Test
    public void mentorNotifiedPullReviewHasAppropriateEvent() {
        PullReview pullReview = PullReviewTest.createTestInstance(State.OPEN);

        pullReview = PullReviewModifier.notifyMentor(pullReview.repositoryId, pullReview.pullRequestId,
                TEST_LINK_TO_EXTERNAL_INFO);

        assertTrue("Could not find PullReviewEventType",
                pullReviewContainsEvent(pullReview, PullReviewEventType.MENTOR_NOTIFIED));
    }

    @Test
    public void generatedPullReviewHasAppropriateEvent() {
        PullReview pullReview = PullReviewTest.createTestInstance(State.NULL);

        pullReview = PullReviewModifier.pullReviewGenerated(pullReview.repositoryId, pullReview.pullRequestId,
                TEST_LINK_TO_EXTERNAL_INFO);

        assertTrue("Could not find PullReviewEventType",
                pullReviewContainsEvent(pullReview, PullReviewEventType.PULL_REVIEW_GENERATED));
    }

    @Test
    public void modifiedPullReviewHasAppropriateEvent() {
        PullReview pullReview = PullReviewTest.createTestInstance(State.OPEN);

        pullReview = PullReviewModifier.updatePullReview(pullReview.repositoryId, pullReview.pullRequestId,
                TEST_LINK_TO_EXTERNAL_INFO);

        assertTrue("Could not find Modified PullReviewEventType",
                pullReviewContainsEvent(pullReview, PullReviewEventType.PULL_REVIEW_MODIFIED));
    }

    private boolean pullReviewContainsEvent(PullReview pullReview, PullReviewEventType pullReviewEventType) {
        boolean foundExpectedEvent = false;
        for (PullReviewEvent pullReviewEvent : pullReview.pullReviewEvents) {
            if (pullReviewEvent.pullReviewEventType != null &&
                    pullReviewEvent.pullReviewEventType.equals(pullReviewEventType)) {
                foundExpectedEvent = true;
                break;
            }
        }
        return foundExpectedEvent;
    }
}
