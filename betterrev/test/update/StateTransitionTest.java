package update;


import models.AbstractPersistenceIntegrationTest;
import models.PullReview;
import models.PullReviewTest;
import models.State;
import org.junit.After;
import org.junit.Test;
import update.external.PullReviewModifier;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class StateTransitionTest extends AbstractPersistenceIntegrationTest {

    public static final String LINK_TO_EXTERNAL_INFO = "linkToExternalInfoText";

    private PullReview pullReview;


    @After
    public void tearDown() {
        pullReview = PullReview.find.byId(pullReview.id);
        pullReview.delete();
    }


    @Test
    public void nullPullReviewGeneratedTransitionsToOpen() {
        pullReview = PullReviewTest.createTestInstance(State.NULL);

        pullReview = PullReviewModifier.pullReviewGenerated(pullReview.repositoryId,
                pullReview.pullRequestId, LINK_TO_EXTERNAL_INFO);

        assertThat(pullReview.state, is(State.OPEN));
    }

    @Test
    public void openPullReviewModifiedTransitionsToOpen() {
        pullReview = PullReviewTest.createTestInstance(State.OPEN);

        pullReview = PullReviewModifier.updatePullReview(pullReview.repositoryId,
                pullReview.pullRequestId, LINK_TO_EXTERNAL_INFO);

        assertThat(pullReview.state, is(State.OPEN));
    }

    @Test(expected = IllegalStateException.class)
    public void closedPullReviewModifiedThrowsException() {
        pullReview = PullReviewTest.createTestInstance(State.CLOSED);

        pullReview = PullReviewModifier.updatePullReview(pullReview.repositoryId,
                pullReview.pullRequestId, LINK_TO_EXTERNAL_INFO);

        assertThat(pullReview.state, is(State.OPEN));
    }

    @Test
    public void openMentorNotifiedTransitionsToPendingApproval() {
        pullReview = PullReviewTest.createTestInstance(State.OPEN);

        pullReview = PullReviewModifier.notifyMentor(pullReview.repositoryId,
                pullReview.pullRequestId, LINK_TO_EXTERNAL_INFO);

        assertThat(pullReview.state, is(State.PENDING_APPROVAL));
    }

    @Test
    public void openTerminatedTransitionsToClosed() {
        pullReview = PullReviewTest.createTestInstance(State.OPEN);

        pullReview = PullReviewModifier.terminatePullReview(pullReview.repositoryId,
                pullReview.pullRequestId, LINK_TO_EXTERNAL_INFO);

        assertThat(pullReview.state, is(State.CLOSED));
    }

    @Test
    public void pendingApprovalApprovedTransitionsToAccepted() {
        pullReview = PullReviewTest.createTestInstance(State.PENDING_APPROVAL);

        pullReview = PullReviewModifier.approvePullReview(pullReview.repositoryId,
                pullReview.pullRequestId, LINK_TO_EXTERNAL_INFO);

        assertThat(pullReview.state, is(State.ACCEPTED));
    }

    @Test
    public void pendingApprovalRejectedTransitionsToClosed() {
        pullReview = PullReviewTest.createTestInstance(State.PENDING_APPROVAL);

        pullReview = PullReviewModifier.rejectPullReview(pullReview.repositoryId,
                pullReview.pullRequestId, LINK_TO_EXTERNAL_INFO);

        assertThat(pullReview.state, is(State.CLOSED));
    }

    @Test
    public void acceptedMergedTransitionsToCommitted() {
        pullReview = PullReviewTest.createTestInstance(State.ACCEPTED);

        pullReview = PullReviewModifier.mergePullReview(pullReview.repositoryId,
                pullReview.pullRequestId, LINK_TO_EXTERNAL_INFO);

        assertThat(pullReview.state, is(State.COMMITTED));
    }
}
