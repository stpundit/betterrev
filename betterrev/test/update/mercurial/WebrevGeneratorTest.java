package update.mercurial;

import models.AbstractPersistenceIntegrationTest;
import models.PullReview;
import models.User;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

public class WebrevGeneratorTest extends AbstractPersistenceIntegrationTest {

    private static final String DONT_CARE = "";

    @Test
    public void generatesWebrev() throws IOException {
        User adoptopenjdk = User.findOrCreate("adoptopenjdk", "adoptopenjdk");
        PullReview pullReview = new PullReview("jdk8-jdk", "1", DONT_CARE, DONT_CARE, adoptopenjdk, DateTime.now(), DateTime.now());
        File webrevLocation = pullReview.webrevLocation();

        try {
            WebrevGenerator generator = new WebrevGenerator("adopt");
            generator.onPullReview(pullReview);

            assertTrue(webrevLocation.exists());
            assertTrue(webrevLocation.isDirectory());
            assertTrue(webrevLocation.list().length > 1);
        } finally {
            FileUtils.deleteDirectory(webrevLocation);
        }
    }

}
