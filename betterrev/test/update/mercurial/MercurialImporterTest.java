package update.mercurial;

import com.aragost.javahg.Changeset;
import org.joda.time.DateTime;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MercurialImporterTest {

    private static final String DATE_BEFORE_JAXWS_COMMITS = "2013-05-01T13:34:39.000-07:00";
    private static final MercurialImporter fullImporter = new MercurialImporter("adopt", "jdk8");

    @Test
    public void importerCallsScript() {
        MercurialImporter importer = new MercurialImporter("conf/adopt_stub/", "jdk8");
        assertEquals(5, importer.doImport());
    }

    @Test
    public void checkFormatDate() {
        DateTime dateTime = DateTime.parse(DATE_BEFORE_JAXWS_COMMITS);
        String formatted = fullImporter.formatDate(dateTime);
        assertEquals("> Wed May 01 13:34:39 2013 -0700", formatted);
    }

    @Test
    public void listsChanges() {
        DateTime lastImport = DateTime.parse(DATE_BEFORE_JAXWS_COMMITS);
        Map<String, List<Changeset>> changesets = fullImporter.listChangesets(lastImport);
        List<Changeset> jaxWsChanges = changesets.get("jaxws");
        assertNotNull(jaxWsChanges);
        assertCcontainsChangeset("88838e08e4ef6a254867c8126070a1975683108d", jaxWsChanges);
    }

    private void assertCcontainsChangeset(String nodeId, List<Changeset> jaxWsChanges) {
        boolean foundChangeset = false;
        for (Changeset changeset : jaxWsChanges) {
            if (nodeId.equals(changeset.getNode()))
                foundChangeset = true;
        }
        assertTrue("Unable to find known changeset in repository", foundChangeset);
    }

}
