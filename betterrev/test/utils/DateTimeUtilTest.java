package utils;

import models.AbstractPersistenceIntegrationTest;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import org.joda.time.DateTime;

import org.junit.Test;

/**
 * Basic test for DateTimeUtil
 * @see utils.DateTimeUtil
 * @see utils.Pattern
 *
 */
public class DateTimeUtilTest{

    @Test
    public void format_WithValidInputs(){
        assertThat((DateTimeUtil.format(new DateTime())),is(not(nullValue())));
    }

    @Test
    public void format_WithValidInputDateTimeAndDefaultPatterns (){
        assertThat((DateTimeUtil.format(new DateTime(),Pattern.DEFAULT)),is(not(nullValue())));
    }

    @Test(expected = NullPointerException.class)
    public void format_WithNullDateTimeAndDefaultPatterns (){
        assertThat((DateTimeUtil.format(null,Pattern.DEFAULT)),is(not(nullValue())));
    }

    @Test(expected = NullPointerException.class)
    public void format_WithValidDateTimeAndNullPatterns (){
        assertThat((DateTimeUtil.format(new DateTime(),null)),is(not(nullValue())));
    }

    @Test(expected = NullPointerException.class)
    public void format_WithNullInputsAsDateTimeAndPatterns (){
        assertThat((DateTimeUtil.format(null,null)),is(not(nullValue())));
    }

}
