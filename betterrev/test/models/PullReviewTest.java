package models;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * PullReview entity basic persistence tests.
 */
public class PullReviewTest extends AbstractPersistenceIntegrationTest {

    private static final String TEST_REPOSITORY_ID = "123";
    private static final String TEST_PULL_REQUEST_ID = "123";
    private static final String TEST_PULL_REVIEW_NAME = "Major Mods!";
    private static final String TEST_PULL_REVIEW_DESCRIPTION = "Some change to the code";
    private static final DateTime TEST_PULL_REVIEW_CREATED_ON = DateTime.now();
    private static final DateTime TEST_PULL_REVIEW_UPDATED_ON = DateTime.now();


    public static PullReview createTestInstance() {
        PullReview pullReview = new PullReview(TEST_REPOSITORY_ID, TEST_PULL_REQUEST_ID,
                TEST_PULL_REVIEW_NAME, TEST_PULL_REVIEW_DESCRIPTION, UserTest.createTestInstance(),
                TEST_PULL_REVIEW_CREATED_ON, TEST_PULL_REVIEW_UPDATED_ON);

        pullReview.tags.add(TagTest.createTestInstance());
        pullReview.mentors.add(MentorTest.createTestInstance());
        pullReview.pullReviewEvents.add(new PullReviewEvent(PullReviewEventType.PULL_REVIEW_GENERATED));

        return pullReview;
    }

    public static PullReview createTestInstance(State state) {
        PullReview pullReview = PullReviewTest.createTestInstance();
        pullReview.state = state;
        pullReview.save();

        return pullReview;
    }

    @Test
    public void validPullReviewPersistsCorrectly() {
        PullReview pullReview = createTestInstance();
        assertThat(pullReview.id, is(nullValue()));
        pullReview.save();

        assertThat(pullReview.id, is(not(nullValue())));
    }

    @Test
    public void validPullReviewCreatedOnSetCorrectly() {
        PullReview pullReview = createTestInstance();
        pullReview.save();

        assertThat(pullReview.createdOn, is(notNullValue()));
    }

    @Test
    public void validPullReviewCreatedWithGeneratedEvent() {
        PullReview pullReview = createTestInstance();
        pullReview.save();

        PullReviewEvent firstPullReviewEvent = pullReview.pullReviewEvents.iterator().next();
        assertThat(firstPullReviewEvent.pullReviewEventType, is(PullReviewEventType.PULL_REVIEW_GENERATED));
    }

    @Test
    public void validPullReviewCreatedWithStateNull() {
        PullReview pullReview = createTestInstance();
        pullReview.save();

        assertThat(pullReview.state, is(State.NULL));
    }
}
