package models;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Basic Mentor entity persistence tests.
 * TODO: Tidy tests so they have one assert each
 */
public class MentorTest extends AbstractPersistenceIntegrationTest {

    private static final String TEST_MENTOR_NAME = "Test Mentor";
    private static final String TEST_MENTOR_EMAIL = "mentor@java.net";
    private static final Mentor.MentorType TEST_MENTOR_TYPE = Mentor.MentorType.INDIVIDUAL;
    private static final int ZERO = 0;

    private static final Logger LOGGER = LoggerFactory.getLogger(MentorTest.class);

    private static final String MENTOR_DATA_SQL_FILE = "conf/evolutions/default/2.sql";
    private static final String UPS_TAG = "# --- !Ups";
    private static final String DOWNS_TAG = "# --- !Downs";

    private static final String ERROR_OCCURRED_WHILST_READING_FILE_MSG = "Error [%s] occurred whilst reading file %s.";

    private static final String REPO_CORBA = "corba";
    private static final String REPO_HOTSPOT = "hotspot";
    private static final String ANY_CLASS_FILE_01 = "./Test.java";
    private static final String ANY_TEST_CLASS_FILE = "./test/compiler/6775880/Test.java";
    private static final String HOTSPOT_MENTOR_NAME = "HotSpot";
    private static final String HOTSPOT_MAILIST_NAME = "hotspot-dev";

    private static final String EMPTY_STRING = "";

    public static Mentor createTestInstance() {
        Mentor mentor = new Mentor(TEST_MENTOR_NAME, TEST_MENTOR_EMAIL, TEST_MENTOR_TYPE);
        mentor.interests.add(InterestTest.createTestInstance());
        return mentor;
    }

    private static void loadMentors() {
        String dropScript = ddl.generateDropDdl();
        ddl.runScript(false, dropScript);

        String createScript = ddl.generateCreateDdl();
        ddl.runScript(false, createScript);

        String updateDataScript = loadMentorsScriptFromFile();
        updateDataScript = removeHeadersFromScriptFile(updateDataScript);
        ddl.runScript(false, updateDataScript);
    }

    @Before
    public void setUp() {
        loadMentors();
    }

    @Test @Ignore
    public void save_validMentor_mentorPersisted() {
        Mentor mentor = createTestInstance();
        assertThat(mentor.id, is(nullValue()));
        mentor.save();
        assertThat(mentor.id, is(not(nullValue())));
    }

    @Test
    public void findsRelevantMentorsForCorba() {
        List<Mentor> mentors = Mentor.findRelevantMentors(REPO_CORBA, ANY_CLASS_FILE_01);
        assertThat(mentors.size(), is(not(equalTo(ZERO))));
    }

    @Test
    public void findsRelevantMentorsForHotspot() {
        List<Mentor> mentors = Mentor.findRelevantMentors(REPO_HOTSPOT, ANY_TEST_CLASS_FILE);
        assertThat(mentors.size(), is(not(equalTo(ZERO))));
    }

    @Test
    public void verifyNameOnAMentorForHotspot() {
        List<Mentor> mentors = Mentor.findRelevantMentors(REPO_HOTSPOT, ANY_TEST_CLASS_FILE);
        Mentor mentor = mentors.get(ZERO);
        assertEquals(HOTSPOT_MENTOR_NAME, mentor.name);
    }

    @Test
    public void verifyEmailOnAMentorForHotspot() {
        List<Mentor> mentors = Mentor.findRelevantMentors(REPO_HOTSPOT, ANY_TEST_CLASS_FILE);
        Mentor mentor = mentors.get(ZERO);
        assertEquals(HOTSPOT_MAILIST_NAME, mentor.email);
    }

    private static String removeHeadersFromScriptFile(String scriptText) {
        String result = scriptText;
        result = result.replace(UPS_TAG, EMPTY_STRING);
        result = result.replace(DOWNS_TAG, EMPTY_STRING);
        return result;
    }

    private static String loadMentorsScriptFromFile() {
        return readTextFile(MENTOR_DATA_SQL_FILE);
    }

    private static String readTextFile(String filenameWithPath) {
        String dataRead = EMPTY_STRING;
        try ( FileReader fileReader = new FileReader(filenameWithPath);
              BufferedReader bufferedReader = new BufferedReader(fileReader);
        ) {
            dataRead = bufferedReader.readLine();
            String eachLine;
            while ((eachLine = bufferedReader.readLine()) != null) {
                dataRead = dataRead + eachLine + System.lineSeparator();
            }
        } catch (IOException e) {
            LOGGER.error(String.format(ERROR_OCCURRED_WHILST_READING_FILE_MSG, e.getMessage(), filenameWithPath), e);
        }
        return dataRead;
    }
}