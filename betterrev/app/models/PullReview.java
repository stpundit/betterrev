package models;

import com.avaje.ebean.validation.NotNull;
import com.google.common.base.Objects;
import org.joda.time.DateTime;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * PullReview entity that is in essence a BetterRev enhanced version of a DVCS pullrequest.
 * <p/>
 * For information here that corresponds to bitbucket information the bitbucket pullrequest
 * is the canonical source.  Eg: createdOn is the creation date of the pull request,
 * not this object.
 */
@Entity
public class PullReview extends Model {

    private static final long serialVersionUID = 188525469548289315L;

    public static Model.Finder<Long, PullReview> find = new Model.Finder<>(Long.class, PullReview.class);

    @Id
    public Long id;

    @NotNull
    @Required
    public String repositoryId;

    @NotNull
    @Required
    public String pullRequestId;

    @NotNull
    @Required
    public String name;

    public String description;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<PullReviewEvent> pullReviewEvents = new HashSet<>();

    @NotNull
    @Enumerated(EnumType.STRING)
    public State state;

    @ManyToMany(cascade = CascadeType.ALL)
    public Set<Tag> tags = new HashSet<>();

    @ManyToOne
    public User requester;

    @ManyToMany(cascade = CascadeType.ALL)
    public Set<Mentor> mentors = new HashSet<>();

    @NotNull
    public DateTime createdOn;

    @NotNull
    public DateTime updatedOn;

    public PullReview(String repositoryId, String pullRequestId, String name, String description, User requester,
                      DateTime createdOn, DateTime updatedOn) {

        this.repositoryId = repositoryId;
        this.pullRequestId = pullRequestId;
        this.name = name;
        this.description = description;
        this.requester = requester;
        this.createdOn = createdOn;
        this.updatedOn = updatedOn;
        this.state = State.NULL;
    }

    public boolean wasUpdatedBefore(DateTime updated) {
        return this.updatedOn.isBefore(updated);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(pullRequestId, repositoryId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;

        PullReview other = (PullReview) obj;
        return Objects.equal(pullRequestId, other.pullRequestId) && Objects.equal(repositoryId, other.repositoryId);
    }

    public String pullRequestUrl() {
        return String.format("https://bitbucket.org/%s/%s/pull-request/%s/diff", requester.bitbucketUserName,
                repositoryId, pullRequestId);
    }

    public String requestersRepositoryUrl() {
        return String.format("ssh://hg@bitbucket.org/%s/%s", requester.bitbucketUserName, repositoryId);
    }

    /**
     * Eg: 'corba', 'hotspot', '.'
     */
    public String openJdkRepoName() {
        String[] split = repositoryId.split("-");

        // Subrepo case
        if (split.length == 2)
            return split[1];

        // Top level repo case
        return ".";
    }

    public static PullReview findByBitbucketIds(String repositoryId, String requestId) {
        return find.where()
                .eq("repositoryId", repositoryId)
                .eq("pullRequestId", requestId)
                .findUnique();
    }

    public File webrevLocation() {
        String relativePath = String.format("public/webrevs/webrev-%s-%s/", repositoryId, pullRequestId);
        return new File(relativePath).getAbsoluteFile();
    }

}
