package models;

import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Mentor entity class representing a conceptual thing that can sponsor a PullReview,
 * for example, a person, project or mailing list.
 */
@Entity
public class Mentor extends Model {

    private static final long serialVersionUID = 188525469548289315L;

    public static Model.Finder<Long, Mentor> find = new Model.Finder<>(Long.class, Mentor.class);

    public enum MentorType {
        INDIVIDUAL,
        PROJECT,
        LIST
    }

    @Id
    public Long id;

    @Required
    public String name;

    @Email
    @Required
    public String email;

    @Required
    @Enumerated(EnumType.STRING)
    public MentorType mentorType;

    // TODO - https://bitbucket.org/adoptopenjdk/betterrev/issue/5/decide-if-we-should-refine-mentorinterests
    @ManyToMany(cascade = CascadeType.ALL)
    public Set<Interest> interests = new HashSet<>();

    public Date createdDate;

    public Mentor(String name, String email, MentorType mentorType) {
        this.name = name;
        this.email = email;
        this.mentorType = mentorType;
        this.createdDate = new Date();
    }

    public static List<Mentor> findRelevantMentors(String repository, String path) {
        List<Mentor> relevantMentors = new ArrayList<>();
        for (Mentor mentor : find.all()) {
            if (mentor.caresAbout(repository, path))
                relevantMentors.add(mentor);
        }
        return relevantMentors;
    }

    private boolean caresAbout(String repository, String path) {
        for (Interest interest : interests) {
            if (interest.caresAbout(repository, path))
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Mentor [id=" + id + ", name=" + name + ", email=" + email + ", mentorType=" + mentorType + ", interests=" + interests
                + ", createdDate=" + createdDate + "]";
    }

}
