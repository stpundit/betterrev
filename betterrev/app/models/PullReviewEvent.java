package models;

import org.joda.time.DateTime;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * PullReviewEvent that represents a lifecycle event associated with a specific PullReview.
 */
@Entity
public class PullReviewEvent extends Model {

    private static final long serialVersionUID = 188525469548289315L;

    public static Model.Finder<Long, PullReviewEvent> find = new Model.Finder<>(Long.class, PullReviewEvent.class);

    
    @Id
    public Long id;

    @ManyToOne
    public PullReview pullReview;

    @Enumerated(EnumType.STRING)
    public PullReviewEventType pullReviewEventType;

    public String linkToExternalInfo;

    public DateTime createdDate;

    private PullReviewEvent() {
        this.createdDate = new DateTime();
    }


    public PullReviewEvent(PullReviewEventType pullReviewEventType) {
        this();
        this.pullReviewEventType = pullReviewEventType;
    }


    public PullReviewEvent(PullReviewEventType pullReviewEventType, String linkToExternalInfo) {
        this(pullReviewEventType);
        this.linkToExternalInfo = linkToExternalInfo;
    }
}
