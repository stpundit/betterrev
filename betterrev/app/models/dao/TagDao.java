package models.dao;

import models.Tag;
import play.utils.dao.BasicDAO;

public class TagDao extends BasicDAO<Long, Tag> {
    public TagDao() {
        super(Long.class, Tag.class);
    }
}
