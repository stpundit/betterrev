package update;

import models.PullReview;

public interface PullReviewListener {

    public void onPullReview(PullReview pullReview);

}
