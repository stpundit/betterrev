package update.bitbucket;

import models.PullReview;
import models.User;
import org.codehaus.jackson.JsonNode;
import org.joda.time.DateTime;
import update.PullReviewListeners;
import update.external.PullReviewModifier;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This Class is responsible for importing BitBucket pullrequests into the Betterrev application.
 */
public class PullRequestImporter {

    public static List<PullReview> importAllReviews(JsonNode response, String repositoryId) {
        JsonNode values = response.get("values");
        Iterator<JsonNode> elements = values.getElements();
        List<PullReview> reviews = new ArrayList<>();
        while (elements.hasNext()) {
            reviews.add(importReview(elements.next(), repositoryId));
        }
        return reviews;
    }

    private static PullReview importReview(JsonNode requestNode, String repositoryId) {
        String requestId = requestNode.get("id").asText();
        PullReview review = PullReview.findByBitbucketIds(repositoryId, requestId);
        DateTime updatedOn = getDateTime(requestNode, "updated_on");
        if (review == null) {
            return createPullReview(requestNode, repositoryId, requestId, updatedOn);
        } else {
            ensurePullReviewUpdated(requestNode, updatedOn, review);
            return review;
        }
    }

    private static void ensurePullReviewUpdated(JsonNode requestNode, DateTime updatedOn, PullReview review) {
        if (!review.wasUpdatedBefore(updatedOn))
            return;

        review.updatedOn = updatedOn;
        review.name = getTitle(requestNode);
        review.description = getDescription(requestNode);
        review.save();

        PullReviewModifier.updatePullReview(review.repositoryId, review.pullRequestId, "");
    }

    private static PullReview createPullReview(JsonNode requestNode, String repositoryId, String requestId, DateTime updatedOn) {
        JsonNode userNode = requestNode.get("user");
        String bitbucketUserName = userNode.get("username").asText();
        String displayName = userNode.get("display_name").asText();
        User user = User.findOrCreate(bitbucketUserName, displayName);

        DateTime createdOn = getDateTime(requestNode, "created_on");
        PullReview review = new PullReview(repositoryId,
                requestId,
                getTitle(requestNode),
                getDescription(requestNode),
                user,
                createdOn,
                updatedOn);
        review.save();

        PullReviewListeners.onPullReview(review);


        PullReviewModifier.pullReviewGenerated(review.repositoryId, review.pullRequestId, "");

        return review;
    }

    private static DateTime getDateTime(JsonNode requestNode, String field) {
        String dateString = requestNode.get(field)
                .asText()
                .replace(' ', 'T');
        return DateTime.parse(dateString);
    }

    private static String getDescription(JsonNode requestNode) {
        return requestNode.get("description").asText();
    }

    private static String getTitle(JsonNode requestNode) {
        return requestNode.get("title").asText();
    }

}
