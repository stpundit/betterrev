package update;

import models.PullReview;
import utils.NullPullReviewListener;

public class PullReviewListeners {

    private static final PullReviewListener defaultListener;

    static {
        // TODO: enable this code when we've properly configured the repositories. 
//        defaultListener = Play.application().isTest()
//        ? new NullPullReviewListener()
//        : new WebrevGenerator(null);
        defaultListener = new NullPullReviewListener();
    }

    public static void onPullReview(PullReview pullReview) {
        defaultListener.onPullReview(pullReview);
    }

}
