package update.mercurial;

import models.PullReview;
import play.Logger;
import update.PullReviewListener;
import utils.Processes;

import java.io.IOException;

/**
 * Generates webrevs based upon a pullreview diffed against the current openjdk repository
 */
public class WebrevGenerator implements PullReviewListener {

    private static final String WEBREV_SCRIPT = "adopt/generate_webrev.sh";

    private String adoptDirectory;

    public WebrevGenerator(String adoptDirectory) {
        this.adoptDirectory = adoptDirectory;
    }

    @Override
    public void onPullReview(PullReview pullReview) {
        try {
            String repository = pullReview.openJdkRepoName();
            String remote = pullReview.requestersRepositoryUrl();
            String resultLocation = pullReview.webrevLocation().getPath();
            int exitCode = Processes.runThroughShell(adoptDirectory, WEBREV_SCRIPT, repository, remote, resultLocation);

            if (exitCode == 0) {
                // TODO #30: create event for web rev generated
            }

        } catch (IOException e) {
            Logger.error(e.getMessage(), e);
        }
    }

}
