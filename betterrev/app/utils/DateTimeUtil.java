package utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * DateTimeUtil class exposes methods to format Joda DateTime to different time formats.
 *
 */
public class DateTimeUtil {
    private DateTimeUtil(){
    }

    /**
     * Method accepts DateTime value of type org.joda.time.DateTime and produce formatted output using Pattern.DEFAULT
     *
     * @param dateTime of type org.joda.time.DateTime
     * @return String
     * @throws NullPointerException
     */
    public static String format(DateTime dateTime) throws NullPointerException{
        if(dateTime == null){
            throw new NullPointerException("Invalid parameter.DateTime could not be null");
        }
        return format(dateTime,Pattern.DEFAULT);
    }

    /**
     * Method accepts DateTime value of type org.joda.time.DateTime and produce formatted output using given Pattern
     * @see utils.Pattern
     *
     * @param dateTime of type org.joda.time.DateTime
     * @param pattern of type util.Pattern
     * @return String
     * @throws NullPointerException
     */
    public static String format(DateTime dateTime, Pattern pattern) throws NullPointerException{
        if(dateTime == null | pattern.toString().isEmpty()){
            throw new NullPointerException("Invalid parameters.Pattern and DateTime could not be null");
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(pattern.toString());
        return dateTimeFormatter.print(dateTime);
    }
}
