package controllers;

import models.Tag;
import models.dao.TagDao;
import play.mvc.Call;
import play.utils.crud.CRUDController;

import static play.data.Form.form;

public class TagController extends CRUDController<Long, Tag> {

    public TagController(TagDao dao) {
        // TODO: 10 is the limit bought back for a single page
        super(dao, form(Tag.class), Long.class, Tag.class, 10, "Name");
    }

    @Override
    protected String templateForList() {
        return "tagList";
    }

    @Override
    protected String templateForForm() {
        return "tagForm";
    }

    @Override
    protected String templateForShow() {
        return "tagShow";
    }

    @Override
    protected Call toIndex() {
        return routes.AdminApplication.tagList();
    }
}
