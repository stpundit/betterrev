package controllers;

import models.dao.TagDao;
import play.mvc.Controller;
import play.mvc.Result;

public class AdminApplication extends Controller {

    private static TagDao tagDAO = new TagDao();
    private static TagController tagController = new TagController(tagDAO);

    public static Result tagList() {
        if (tagController.list(0) == null) {
            return tagController.newForm();
        }
        return tagController.list(0);
    }

    public static Result tagNewForm() {
        return tagController.newForm();
    }

    public static Result tagCreate() {
        return tagController.create();
    }

    public static Result tagEditForm(Long key) {
        return tagController.editForm(key);
    }

    public static Result tagUpdate(Long key) {
        return tagController.update(key);
    }

    public static Result tagDelete(Long key) {
        return tagController.delete(key);
    }

    public static Result tagShow(Long key) {
        return tagController.show(key);
    }
}
